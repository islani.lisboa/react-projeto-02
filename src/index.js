import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'; // tramitacao de componentes


// Funcao com dois parametros
ReactDOM.render(<App />, document.getElementById('root')); 
// (parametro 1 = componente de saída , parametro 2 = referencia)
// Nao pode declarar o componente com letra "minuscula" inicial