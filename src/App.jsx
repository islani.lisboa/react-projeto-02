import './style.css'
import { useState } from 'react';


// stateless
const App = () => {
   const [valor1, setV1] = useState("");
   const [valor2, setV2] = useState("");
   const [resultado, setResult] = useState(0);

   function novoItem(e) {setV1(e.target.value)};
   function novoItem2(e) {setV2(e.target.value)};

   function Somar() {setResult(`${Number(valor1)+Number(valor2)}`)};
   function Subtrair() {setResult(`${Number(valor1)-Number(valor2)}`)};
   function Multiplicar() {setResult(`${Number(valor1)*Number(valor2)}`)};
   function Dividir() {setResult(`${Number(valor1)/Number(valor2)}`)};

   return (
  
    <div>
      <h2>Calculadora</h2>
      <input type="text" onChange={novoItem} placeholder="Insira o valor 1" value={valor1} />
      <input type="text" onChange={novoItem2} placeholder="Insira o valor 2" value={valor2} />
      <br />
      <br />
      <button className="btn" onClick={() => Somar()}>Somar</button>
      <button className="btn" onClick={() => Subtrair()}>Subtrair</button>
      <button className="btn" onClick={() => Multiplicar()}>Multiplicar</button>
      <button className="btn" onClick={() => Dividir()}>Dividir</button>
      <div>
        <p>Resultado:</p>
        <h2>{resultado}</h2>
      </div>
    </div>
  );
};

export default App;
